# Call Billing

This is a simple program that calculates the total amount of money a user has to pay based on the total number of call blocks used. Each call is divided into blocks of 30 seconds, with any remaining seconds counted as an additional block.

# Requirements
1. Python 3.8
2. Docker
3. Docker Compose


# Getting Started

## 1. Build docker image:
``
sudo docker build -t sanic_va:v1 .
``
## 2. Create a .env file in the root directory with the following environment variables:


DB_HOST=db

DB_PORT=5432

DB_NAME=postgres

DB_USER=postgres

DB_PASSWORD=mypassword



## 3. Start the database and the application by running:

``
sudo docker-compose up -d
``


## 4. Access the application at http://localhost:8000


## 5. Run the server:
python app.py

## 6. To stop the application and the database, run:

``
sudo docker-compose down
``