# Use an official Python runtime as a parent image
FROM python:3.8.10

# Set the working directory to /app
WORKDIR /app

# Copy the requirements file into the container
COPY requirements.txt .

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Copy the rest of the application code into the container
COPY . .

# Install PostgreSQL
RUN apt-get update && apt-get install -y postgresql postgresql-contrib

# Set up the PostgreSQL database
USER postgres
RUN /etc/init.d/postgresql start &&\
    psql --command "CREATE USER myuser WITH SUPERUSER PASSWORD 'mypassword';" &&\
    createdb -O myuser mydatabase

# Switch back to the root user
USER root

# Make port 8000 available to the world outside this container
EXPOSE 8000

# Define environment variable
ENV DATABASE_URL postgres://myuser:mypassword@localhost/mydatabase

# # Run the command to start Sanic server
# CMD ["python", "app.py"]