from sanic import Sanic
from sanic.request import Request
from sanic.response import json
from collections import defaultdict

app = Sanic(__name__)

# database simulation
users_calls = defaultdict(list)
#Key: user
#Value: List of blocks (each call corresponding a block)

@app.put('/mobile/<username>/call')
#Using the parameters inside the form
async def api_record_call(request: Request, username: str):
    call_duration = int(request.form['call_duration'][0])
    block_count = (call_duration + 29) // 30 # round up to next block
    users_calls[username].append(block_count)
    
    return json({
        'user': username,
        'call_duration': block_count
    })

@app.get('/mobile/<username>/billing')
async def api_get_billing(request: Request, username: str):
    call_count = len(users_calls[username])
    block_count = sum(users_calls[username])
    return json({
        'call_count': call_count,
        'block_count': block_count
    })

if __name__ == '__main__':
    app.run(host='localhost', port=8000)